package tor

import (
	"os"
	"testing"
)

func TestGenerateHashedPassword(t *testing.T) {
	// 16:C15305F97789414B601259E3EC5E76B8E55FC56A9F562B713F3D2BA257
	hp := generateHashedPassword([8]byte{0xC1, 0x53, 0x05, 0xF9, 0x77, 0x89, 0x41, 0x4B}, "examplehashedpassword")
	if hp != "16:C15305F97789414B601259E3EC5E76B8E55FC56A9F562B713F3D2BA257" {
		t.Fatalf("hashed passwords do not match. Expected %s, got %s", "16:C15305F97789414B601259E3EC5E76B8E55FC56A9F562B713F3D2BA257", hp)
	}
}

func TestGenerateTorrc(t *testing.T) {
	path := "./torrc.test"
	password := "examplehashedpassword"
	err := NewTorrc().WithHashedPassword(password).Build(path)
	if err != nil {
		t.Errorf("Torrc file could not be written")
	}
	os.Remove(path)
}
