package tor

import (
	"fmt"
	"git.openprivacy.ca/openprivacy/log"
	"path"
	"testing"
)

func getStatusCallback(progChan chan int) func(int, string) {
	return func(prog int, status string) {
		fmt.Printf("%v %v\n", prog, status)
		progChan <- prog
	}
}

func TestTorProvider(t *testing.T) {
	progChan := make(chan int)
	log.SetLevel(log.LevelDebug)
	torpath := path.Join("..", "tmp/tor")
	log.Debugf("setting tor path %v", torpath)
	acn, err := NewTorACNWithAuth(path.Join("../testing/"), torpath, 9051, HashedPasswordAuthenticator{"examplehashedpassword"})
	if err != nil {
		t.Error(err)
		return
	}
	acn.SetStatusCallback(getStatusCallback(progChan))

	progress := 0
	for progress < 100 {
		progress = <-progChan
		t.Logf("progress: %v", progress)
	}

	acn.Close()
}
