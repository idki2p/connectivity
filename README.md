# connectivity

A library providing an ACN (Anonymous Communication Network
) networking abstraction

## Supported ACNs

* Tor v3 Onion Services

## Requirements for ACN Support

* Reference an EndPoint via a string / hostname
* Maintain an endpoint via a PublicKey (the underlying crypto is the
 responsibility of the implementation)
 
## Using

Each ACN implementation provides a specific start function that takes in the
required parameters to e.g. find a specific binary on the system, attempt to
 talk to a specific system service or launch an in-memory networking manager:

        acn, err := NewTorACN(".", "", 9051, HashedPasswordAuthenticator{"examplehasedpassword"})
        if err != nil {
            t.Error(err)
            return
        }

At this point the ACN is responsible for setting up the networking interface,
the result of which can be checked via the Status callback:
        
    
        acn.SetStatusCallback(getStatusCallback(progChan))
    
        progress := 0
        for progress < 100 {
            progress = <-progChan
        }

Once initialized the ACN can be used to open new connections:

        conn,err := acn.Open(hostname);

Or host a service on the ACN:

        ls,err := acn.Listen(identity, port) ;

We also provide closing and restart functionality for managing the networking
service:

        acn.Restart()
and
        
        acn.Close()