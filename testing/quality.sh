#!/bin/sh

echo "Checking code quality (you want to see no output here)"

echo "Formatting:"
gofmt -s -w -l .

echo "Vetting:"
go list ./... | xargs go vet

echo ""
echo "Linting:"

go list ./... | xargs golint

# ineffassign (https://github.com/gordonklaus/ineffassign)
echo "Checking for ineffectual assignment of errors (unchecked errors...)"
ineffassign .

# misspell (https://github.com/client9/misspell)
echo "Checking for misspelled words..."
go list ./... | xargs misspell
