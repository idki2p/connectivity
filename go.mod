module git.openprivacy.ca/openprivacy/connectivity

go 1.13

require (
	git.openprivacy.ca/openprivacy/bine v0.0.4
	git.openprivacy.ca/openprivacy/log v1.0.1
	golang.org/x/crypto v0.0.0-20201012173705-84dcc777aaee
)
